
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Visualiza Orçamento
        <small>#007612</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Visualizar Orçamento</li>
    </ol>
</section>



<!-- Main content -->
<section class="invoice">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <i class="fa fa-globe"></i> Nome da Minha empresa
                <small class="pull-right">Date: 03/10/2015</small>
            </h2>
        </div><!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            <h4> Empresa</h4>
            <address>
                <strong>Nome da Minha empresa.</strong><br>
                Rua Fulando de Tal<br>
                Nº: 123<br>
                Bairro Teste<br>
                Estado: CEARA<br>
                Fone: (804) 123-5432<br>
                Email: info@brunogomes.com.br
            </address>
        </div><!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <h4>Cliente</h4>
            <address>
                <strong> Nome do CLiente</strong><br>
                Rua Fulando de Tal<br>
                Nº: 123<br>
                Bairro Teste<br>
                Estado: CEARA<br>
                Fone: (804) 123-5432<br>
                Email: info@brunogomes.com.br
            </address>
        </div><!-- /.col -->

    </div><!-- /.row -->

    <!-- Table row -->
    <div class="row">
        <div class="col-md-12">

            <table class="table table-bordered table-striped">
                <tbody><tr class="bg-gray">
                        <th style="width: 10px">#</th>
                        <th>Serviço</th>

                        <th class="text-center col-md-2"> Subtotal </th>
                    </tr>
                    <tr>
                        <td class= " text-center">1.</td>
                        <td>Update software</td>

                        <td class=" text-center">150,00</td>
                    </tr>

                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2" style="text-align: right"><strong>Total:</strong></td>
                        <td><strong>R$ 50,00</strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div><!-- /.row -->
    <div class="row">
        <div class="col-md-12">

            <table class="table table-bordered table-striped">
                <tbody><tr class="bg-gray">
                        <th style="width: 10px">#</th>
                        <th>Produtos</th>
                        <th class="text-center col-md-2">Quantidade</th>
                        <th class="text-center col-md-2"> Subtotal </th>
                    </tr>
                    <tr>
                        <td>1.</td>
                        <td>Update software</td>
                        <td class=" text-center">2</td>
                        <td class=" text-center">150,00</td>
                    </tr>

                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3" style="text-align: right"><strong>Total:</strong></td>
                        <td><strong>R$ 50,00</strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
            <p class="lead">Observações:</p>

            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
            </p>
        </div><!-- /.col -->
        <div class="col-xs-6">
            <p class="lead">Valores:</p>
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th style="width:50%">Total de Serviço:</th>
                        <td>R$ 250.30</td>
                    </tr>
                    <tr>
                        <th>Total de Produtos</th>
                        <td>R$ 10.34</td>
                    </tr>
                    <tr>
                        <th>Total do Orçamento:</th>
                        <td>$265.24</td>
                    </tr>

                </table>
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->


    <!-- this row will not appear when printing -->
    <div class="row no-print">
        <div class="col-xs-12">
            <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Imprimir</a>

            <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Gerar PDF</button>
        </div>
    </div>
</section><!-- /.content -->
<div class="clearfix"></div>

