
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Orçamentos
        <small>Listagem</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Orçamentos Registrados</h3>

                    <div class="pull-right box-tools">
                        <a class="btn btn-primary btn-sm" href="cadastro_clientes.php" ><i class="fa fa-plus"> <strong>Novo</strong></i></a>
                    </div>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                    <div class="box-body ">
                        <table class="table table-bordered">
                            <tbody><tr class="bg-light-blue">
                                    <th style="width: 10px">#</th>
                                    <th>Cliente</th>
                                    <th class="text-center col-md-2">Valor</th>
                                    <th class="text-center col-md-2">Data de Inicio</th>
                                    <th class="text-center col-md-2">Status</th>
                                    <th class="text-center col-md-2"> Ações </th>
                                </tr>
                                <tr>
                                    <td>1.</td>
                                    <td>Fulano de Tal</td>
                                    <td>100,00</td>

                                    <td>01/01/2014</td>
                                    <td>Aberta</td>

                                    <td class=" text-center">
                                        <a href="" class="btn btn-success btn-xs"> <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span></a>
                                        <a href="" class="btn btn-primary btn-xs"> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                        <a href="" class="btn btn-danger btn-xs"> <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2.</td>
                                    <td>Fulano de Tal</td>
                                    <td>100,00</td>
                                    <td>01/01/2014</td>
                                    <td>Aberta</td>
                                    <td class=" text-center">
                                        <a href="" class="btn btn-success btn-xs"> <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span></a>
                                        <a href="" class="btn btn-primary btn-xs"> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                        <a href="" class="btn btn-danger btn-xs"> <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3.</td>
                                    <td>Fulano de Tal</td>
                                    <td>100,00</td>
                                    <td>01/01/2014</td>
                                    <td>Aberta</td>
                                    <td class=" text-center">
                                        <a href="" class="btn btn-success btn-xs"> <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span></a>
                                        <a href="" class="btn btn-primary btn-xs"> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                        <a href="" class="btn btn-danger btn-xs"> <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4.</td>
                                    <td>Fulano de Tal</td>
                                    <td>100,00</td>
                                    <td>01/01/2014</td>
                                    <td>Aberta</td>

                                    <td class=" text-center">
                                        <a href="" class="btn btn-success btn-xs"> <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span></a>
                                        <a href="" class="btn btn-primary btn-xs"> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                        <a href="" class="btn btn-danger btn-xs"> <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div><!-- /.box-body -->

                    <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <li><a href="#">«</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">»</a></li>
                        </ul>
                    </div>
                </form>
            </div><!-- /.box -->

        </div>
    </div><!-- /.row -->
    <!-- Main row -->


</section><!-- /.content -->
