
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Serviços
        <small>Cadastro</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Cadastro de Serviços</h3>

                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                    <div class="box-body">
                        <div class="row">
                            <br>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Serviço:</label> 
                                    <input type="text" class="form-control input-sm" name="nome" value="">
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Valor:</label> 
                                    <div class="input-group">
                                        <span class="input-group-addon input-sm">R$</span>
                                        <input type="text" class="form-control input-sm">

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-7">
                                <div class="form-group">
                                    <label>Descrição do Serviço:</label>
                                    <textarea class="form-control input-sm" name="obs_cliente" rows="6"></textarea>
                                </div>
                            </div>
                        </div>



                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class=" btn btn-success">Salvar</button>

                        <button type="reset" class="btn btn-default">Limpar</button>

                        <button type="button" class="btn btn-danger">Cancelar</button>

                    </div>
            </div>
            </form>
        </div><!-- /.box -->

    </div>
</div><!-- /.row -->
<!-- Main row -->


</section><!-- /.content -->
