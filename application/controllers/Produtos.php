<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Produtos extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('template/header');
        $this->load->view('produtos/listar-produtos');
        $this->load->view('template/footer');
    }
    public function cadastrar() {
        $this->load->view('template/header');
        $this->load->view('produtos/cadastro-produto');
        $this->load->view('template/footer');
    }

}
