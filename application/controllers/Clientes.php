<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Clientes extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('template/header');
        $this->load->view('clientes/listar-clientes');
        $this->load->view('template/footer');
    }
    public function cadastrar() {
        $this->load->view('template/header');
        $this->load->view('clientes/cadastrar-clientes');
        $this->load->view('template/footer');
    }
    public function atualizar() {
        $this->load->view('template/header');
        $this->load->view('clientes/atualizar-clientes');
        $this->load->view('template/footer');
    }
    

}
